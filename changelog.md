# Changelog - Dart Unicode Helper

## 0.2.0 2013-12-27

* Using SparseList (http://pub.dartlang.org/packages/sparse_list) for Character List.

## 0.1.2 2013-12-26

* Add this changelog.md file.
* Add hop runner file.
* Add gh-pages.


